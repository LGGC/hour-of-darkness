
state={
	id=319
	name="STATE_319"
	resources={
		chromium=1.000
	}

	history={
		owner = DOM
		victory_points = {
			7660 5 
		}
		victory_points = {
			4598 3
		}
		victory_points = {
			10484 2
		}
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 3
			air_base = 1
			7660 = {
				naval_base = 2

			}

		}
		add_core_of = DOM

	}

	provinces={
		4598 7632 7660 10484 13706 13707 
	}
	manpower=1456000
	buildings_max_level_factor=1.000
	state_category=town
}


state={
	id=778
	name="STATE_778"
	resources={
		steel=4.000
	}

	history={
		owner = USA
		victory_points = {
			3712 2 
		}
		buildings = {
			infrastructure = 4
			arms_factory = 1

		}
		add_core_of = USA

	}

	provinces={
		733 3712 3715 10481 
	}
	manpower=481000
	buildings_max_level_factor=1.000
	state_category=rural
}

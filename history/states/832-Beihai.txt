state={
	id=832
	name="STATE_832"
	
	history={
		owner = GND
		add_core_of = GND
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 3
		}
		victory_points = {
			1018 1 
		}
		1950.1.1 = {
			owner = PRC
		}
	}	
	
	provinces={
		1018 4023 7192 10004 11926 
	}
	manpower=4830000
	buildings_max_level_factor=1.000
	state_category=city
}


state={
	id=198
	name="STATE_198"

	history={
		owner = RUS
		buildings = {
			infrastructure = 5
			air_base = 3

		}
		add_core_of = RUS
		add_core_of = UKR
		1942.11.21 = {
			owner = RUK

		}
		1944.6.20 = {
			controller = RUS

		}
		1946.1.1 = {
			owner = RUS

		}
		2000.1.1 = {
			owner = UKR
			controller = UKR
			remove_core_of = RUS

		}

	}

	provinces={
		6480 9481 9533 11464 11490 
	}
	manpower=1893391
	buildings_max_level_factor=1.000
	state_category=large_town
}

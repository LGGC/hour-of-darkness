
state={
	id=818
	name="STATE_818"
	resources={
		chromium=4.000
		aluminium=14.000
	}

	history={
		owner = OTT
		victory_points = {
		    9874 2
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = ALB
		add_core_of = SER
		add_core_of = KOS
		1913.1.1 = {
			owner = SER
		}
	}

	provinces={
		6940 9849 9874 13598 
	}
	manpower=53000
	buildings_max_level_factor=1.000
	state_category=pastoral
}

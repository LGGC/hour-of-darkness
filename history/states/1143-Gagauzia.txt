state={
	id=1143
	name="STATE_1143"
	
	history={
		owner = RUS
		victory_points = {
			13675 1
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = ROM
		add_core_of = MOL
		add_core_of = GAG
		1941.6.22 = {
			owner = RUS
			add_core_of = RUS

		}
		1942.11.22 = {
			controller = ROM

		}
		1945.1.1 = {
			controller = RUS

		}
		1946.1.1 = {
			remove_core_of = ROM

		}
		1990.1.1 = {
			owner = GAG

		}
		2000.1.1 = {
			owner = MOL
			remove_core_of = RUS

		}

	}
	
	provinces={
		13674 13675 
	}
	manpower=134535
	buildings_max_level_factor=1.000
	state_category=pastoral
}


state={
	id=161
	name="STATE_161"
	resources={
		aluminium=3
	}

	history={
		owner = ITA
		victory_points = {
			6606 5
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			air_base = 2
			dockyard = 2
			782 = {
				naval_base = 2

			}

		}
		add_core_of = ITA
		1944.6.20 = {
			owner = RSI

		}
		1945.1.1 = {
			ITA = {
				set_province_controller = 9924
				
			}

		}
		1946.1.1 = {
			owner = ITA

		}

	}

	provinces={
		782 6606 6793 6985 9924 
	}
	manpower=2134934
	buildings_max_level_factor=1.000
	state_category=city
}

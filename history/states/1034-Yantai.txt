
state={
	id=1034
	name="STATE_1034"

	history={
		owner = CHI
		
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			1104 3
		}
		buildings = {
			infrastructure = 3
			air_base = 3
		}
		1938.10.25 = {
			owner = CHI
			controller = EHB
			remove_core_of = SHD
		}
		1940.1.1 = {
			owner = RNG
			controller = RNG
		}	
		1946.1.1 = {
			owner = PRC
			controller = PRC
			PRC = {
				set_province_controller = 7129
			}
		}
	}

	provinces={
		1200 4186 7105 10018 1104
	}
	manpower=70200
	buildings_max_level_factor=1.000
	state_category=town
}

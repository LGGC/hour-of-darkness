
state={
	id=200
	name="STATE_200"

	history={
		owner = RUS
		victory_points = {
			11405 1 
		}
		buildings = {
			infrastructure = 5
			air_base = 3

		}
		add_core_of = RUS
		add_core_of = UKR
		1939.1.1 = {
			buildings = {
				industrial_complex = 1

			}

		}
		1942.11.21 = {
			owner = RUK

		}
		1944.6.20 = {
			controller = RUS

		}
		1946.1.1 = {
			owner = RUS

		}
		2000.1.1 = {
			owner = UKR
			controller = UKR
			remove_core_of = RUS

		}

	}

	provinces={
		429 588 3399 3449 3767 6420 6776 9571 11405 
	}
	manpower=1294082
	buildings_max_level_factor=1.000
	state_category=town
}

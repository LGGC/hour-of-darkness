state={
	id=1358
	name="STATE_1358"

	history={
		owner = JAP
		victory_points = {
			13037 1
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = JAP
		1946.1.1 = {
			owner = RUS
			add_core_of = RUS
			set_state_name = Yuzhno-Kurilsk
			set_province_name = { id = 13037 name = "Yuzhno-Kurilsk" }

		}

	}

	provinces={
		13037 
	}
	manpower=9501
	buildings_max_level_factor=1.000
	state_category=tiny_island
}

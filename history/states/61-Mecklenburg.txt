
state={
	id=61
	name="STATE_61"

	history={
		owner = GER
		victory_points = {
			321 4
		}
		victory_points = {
			11276 3
		}
		victory_points = {
			11305 2
		}
		victory_points = {
			3258 2
		}
		buildings = {
			infrastructure = 6
			arms_factory = 2
			industrial_complex = 1
			321 = {
				naval_base = 6
			}
			air_base = 3

		}
		add_core_of = GER
		1946.1.1 = {
			owner = RUS

		}
		1950.1.1 = {
			owner = DDR

		}
		1991.1.1 = {
			owner = FRG

		}

	}

	provinces={
		268 293 321 3258 9294 11276 11305 
	}
	manpower=1166353
	buildings_max_level_factor=1.000
	state_category=town
}

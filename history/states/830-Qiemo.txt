state={
	id=830
	name="STATE_830"
	
	history = {
		add_core_of = CHI
		add_core_of = PRC	
		add_core_of = SIK
	
		owner = SIK
		
		1936.1.1 = {
			owner = TNG
			add_core_of = TNG
		}
		1938.1.1 = {
			owner = SIK
			controller = SIK
		}
		1950.1.1 = {
			owner = PRC
		}				
		buildings = {
			infrastructure = 2
			industrial_complex = 1

		}
		victory_points = {
			1943 1 
		}
	}	
	
	provinces={
		1943 2074 10315 10911 
	}
	manpower=231980
	buildings_max_level_factor=1.000
	state_category=rural
}

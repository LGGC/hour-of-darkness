state={
	id=1331
	name="STATE_1331"

	history={
		owner = RUS
		victory_points = {
			409 2
		}
		buildings = {
			infrastructure = 4

		}
		add_core_of = RUS
		add_core_of = UKR
		1942.11.21 = {
			owner = RUK

		}
		1944.6.20 = {
			controller = RUS

		}
		1946.1.1 = {
			owner = RUS

		}
		2000.1.1 = {
			owner = UKR
			controller = UKR
			remove_core_of = RUS

		}

	}

	provinces={
		409 434 3403 3452 3468 6451 6478 9573 11409 
	}
	manpower=226491
	buildings_max_level_factor=1.000
	state_category=rural
}

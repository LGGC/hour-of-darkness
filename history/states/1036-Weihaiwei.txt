
state={
	id=1036
	name="STATE_1036"

	history={
		owner = CHI
		
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			4205 1
		}
		buildings = {
			infrastructure = 3
			4205 = {
				naval_base = 2
				coastal_bunker = 1
				bunker = 1

			}
		}
		1938.10.25 = {
			owner = CHI
			controller = EHB
			remove_core_of = SHD
		}
		1940.1.1 = {
			owner = CHI
			controller = JAP
		}	
		1946.1.1 = {
			owner = PRC
			controller = PRC
			PRC = {
				set_province_controller = 7129
			}
		}
	}

	provinces={
		4205
	}
	manpower=300200
	buildings_max_level_factor=1.000
	state_category=town
}

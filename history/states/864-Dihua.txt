state={
	id=864
	name="STATE_864"
	
	history={
		owner = SIK
		add_core_of = SIK
		add_core_of = CHI
		add_core_of = PRC
		
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			
		}
		victory_points = {
			12656 3 
		}
		1950.1.1 = {
			owner = PRC
		}		
	}
	
	provinces={
		12656
	}
	manpower=1780000
	buildings_max_level_factor=1.000
	state_category=town
}

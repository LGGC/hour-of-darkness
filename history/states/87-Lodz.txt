
state={
	id=87
	name="STATE_87"

	history={
		owner = RUS
		victory_points = {
			9508 5
		}
		victory_points = {
			9439 2
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 2

		}
		add_core_of = POL
		1939.1.1 = {
			buildings = {
				air_base = 3

			}

		}
		1940.5.10 = {
			owner = GER
			controller = GER

		}
		1944.6.20 = {
			owner = GER
			controller = GER

		}
		1946.1.1 = {
			owner = POL
			controller = POL

		}

	}

	provinces={
		493 548 584 5095 6401 9439 9508 9546 11515 
	}
	manpower=2632000
	buildings_max_level_factor=1.000
	state_category=town
}

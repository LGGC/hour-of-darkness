
state={
	id=99
	name="STATE_99"
	resources={
		aluminium=6.000
	}

	history={
		owner = DEN
		add_core_of = DEN
		victory_points = {
			6364 3 
		}
		victory_points = {
			6235 2 
		}
		victory_points = {
			399 1 
		}
		buildings = {
			infrastructure = 8
			industrial_complex = 3
			air_base = 2
			394 = {
				naval_base = 3

			}
			6364 = {
				naval_base = 1

			}

		}

	}

	provinces={
		316 394 399 3206 3277 3341 6235 6364 14317 
	}
	manpower=1535000
	buildings_max_level_factor=1.000
	state_category=town
}

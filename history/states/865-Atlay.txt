state={
	id=865
	name="STATE_865"
	
	history = {
		owner = SIK
		add_core_of = SIK
		add_core_of = CHI
		add_core_of = PRC
		victory_points = {
			4823 1 
		}
		buildings = {
			infrastructure = 1
		}
		1946.1.1 = {
			owner = ETS
		}		
		1950.1.1 = {
			owner = PRC
		}
	}
	
	provinces={
		4714 4788 4823 4849 7693 7737 12590 12737
	}
	manpower=321100
	buildings_max_level_factor=1.000
	state_category=town
}

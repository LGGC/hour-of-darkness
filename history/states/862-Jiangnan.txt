state={
	id=862
	name="STATE_862"
	
	history={
		owner = PRC
		add_core_of = CHI
		add_core_of = PRC
		1936.1.1 = {
			owner = CHI
			add_core_of = CHI
			add_core_of = PRC
		}		
		1950.1.1 = {
			owner = PRC
		}		
		buildings = {
			infrastructure = 1
			industrial_complex = 1
			arms_factory = 1
			industrial_complex = 2

		}
		victory_points = {
			12030 1
		}
	}

	provinces={
		1053 4081 10112 1110 11945 12030
	}
	manpower=820453
	buildings_max_level_factor=1.000
	state_category=town
}

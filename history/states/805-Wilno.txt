
state={
	id=805
	name="STATE_805"

	history={
		owner = RUS
		victory_points = {
			3320 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			air_base = 5

		}
		add_core_of = LIT
		1940.5.10 = {
			owner = RUS
			controller = RUS

		}
		1942.11.21 = {
			owner = ROS
			controller = ROS

		}
		1944.6.20 = {
			owner = POL
			controller = GER
		}
		1945.1.1 = {
			owner = RUS
			controller = RUS
		}
		1946.1.1 = {
			owner = RUS
			add_core_of = RUS
			remove_core_of = POL
			set_state_name = Vilnius
			set_province_name = {
				id = 3320
				name = Vilnius

			}

		}
		2000.1.1 = {
			owner = LIT
			remove_core_of = RUS

		}

	}

	provinces={
		3320 9274 9295 11342 
	}
	manpower=487000
	buildings_max_level_factor=1.000
	state_category=rural
}

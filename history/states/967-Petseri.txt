state={
	id=967
	name="STATE_967"
	
	history={
		owner = RUS
		buildings = {
			infrastructure = 2

		}
		add_core_of = EST
		add_core_of = RUS
		1941.6.21 = {
			owner = RUS
			controller = RUS

		}
		1942.11.21 = {
			owner = ROS
			controller = ROS

		}
		1944.6.20 = {
			owner = RUS
			controller = GER
		}
		1945.1.1 = {
			owner = RUS
			controller = RUS
		}
		1946.1.1 = {
			add_core_of = RUS
			remove_core_of = EST
			set_state_name = "Pechory"
		}
	}
	
	provinces={
		13268 13269 
	}
	manpower=4300
	buildings_max_level_factor=1.000
	state_category=enclave
}

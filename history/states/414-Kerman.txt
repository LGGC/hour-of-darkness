
state={
	id=414
	name="STATE_414"
	manpower = 486013

	resources = {
			chromium = 5
	}
	
	state_category = wasteland

	history={
		owner = PER
		buildings = {
			infrastructure = 1
		}
		victory_points = {
			10913 1 
		}
		add_core_of = PER
		add_claim_by = ENG
	}

	provinces={
		1986 7990 10759 10890 10913 11249 12739 12809 12852 12895 
	}
}

state={
	id=1452
	name="STATE_1452"

	history={
		owner = AUS
		victory_points = {
			14672 1
		}
		buildings = {
			infrastructure = 3
			
		}
		add_claim_by = HUN
		add_claim_by = AUS
		add_core_of = CRO
		1941.6.22 = {
			owner = CRO

		}
		1946.1.1 = {
			owner = YUG

		}
		2000.1.1 = {
			owner = CRO
			remove_core_of = YUG

		}
	}

	provinces={
		14672 
	}
	manpower=180117
	buildings_max_level_factor=1.000
	state_category=pastoral
}

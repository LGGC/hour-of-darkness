
state={
	id=201
	name="STATE_201"

	history={
		owner = RUS
		victory_points = {
			11514 2
		}
		buildings = {
			infrastructure = 5
			air_base = 3

		}
		add_core_of = RUS
		add_core_of = UKR
		1942.11.21 = {
			owner = RUK

		}
		1944.6.20 = {
			controller = RUS

		}
		1946.1.1 = {
			owner = RUS

		}
		2000.1.1 = {
			owner = UKR
			controller = UKR
			remove_core_of = RUS

		}

	}

	provinces={
		441 550 3470 3570 6593 9466 9493 11477 11514 
	}
	manpower=1417834
	buildings_max_level_factor=1.000
	state_category=town
}

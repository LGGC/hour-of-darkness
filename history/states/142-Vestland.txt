
state={
	id=142
	name="STATE_142"
	resources={
		chromium=1.000
		aluminium=15.000
		steel=8.000
	}

	history={
		owner = NOR
		victory_points = {
			122 5
		}
		victory_points = {
			53 3
		}
		victory_points = {
			3192 1
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			air_base = 1
			6176 = {
				naval_base = 1
				coastal_bunker = 1

			}
			122 = {
				naval_base = 3
				coastal_bunker = 2

			}

		}
		add_core_of = NOR
		1940.5.10 = {
			controller = GER

		}
		1946.1.1 = {
			controller = NOR

		}

	}

	provinces={
		53 65 122 137 199 3049 3090 3114 3129 3188 3192 6145 6176 6216 9047 9170 11101 11198 11367 
	}
	manpower=880945
	buildings_max_level_factor=1.000
	state_category=town
}

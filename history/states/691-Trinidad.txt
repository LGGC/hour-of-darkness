state={
	id=691
	name="STATE_691"
	manpower = 428119
	resources={
		oil=12 # was: 16
	}
	
	state_category = rural


	history={
		owner = ENG
		victory_points = {
			3284 1
		}
		buildings = {
			infrastructure = 3
			3284 = {
				naval_base = 1
			}
		}
		add_core_of = TRT
		1972.1.1 = {
			owner = TRT

		}
	}
	
	provinces={ 3284 13012 }
}

state= {
	id=904
	name="STATE_904"
	manpower = 1000000
	state_category = city

	history= {
	
		buildings = {
			infrastructure = 2
		}
		victory_points = {
			4939 1 
		}	
		owner = CHI
		add_core_of = CHI
		add_core_of = PRC

		1936.1.1 = {
			owner = PRC
		}
		1946.1.1 = {
			owner = PRC
			controller = PRC
		}	
		1950.1.1 = {
			owner = PRC
		}			
	}
	provinces={
		1458 7314 4939
	}
}

state={
	id=78
	name="STATE_78"

	history={
		owner = RUS
		victory_points = {
			13679 5
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 2
			air_base = 2

		}
		add_core_of = ROM
		add_core_of = MOL
		1941.6.22 = {
			owner = RUS
			add_core_of = RUS

		}
		1942.11.22 = {
			controller = ROM

		}
		1945.1.1 = {
			controller = RUS

		}
		1946.1.1 = {
			remove_core_of = ROM

		}
		2000.1.1 = {
			owner = MOL
			remove_core_of = RUS

		}

	}

	provinces={
		565 3707 3724 6743 11686 11705 13679 
	}
	manpower=2470970
	buildings_max_level_factor=1.000
	state_category=rural
}

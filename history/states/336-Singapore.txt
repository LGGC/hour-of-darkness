
state={
	id=336
	name="STATE_336"
	resources={
		rubber=342.000
		steel=20.000
		tungsten=189.000
		aluminium=15.000
	}

	history={
		owner = ENG
		victory_points = {
			12299 10 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			air_base = 6
			12299 = {
				naval_base = 8
				coastal_bunker = 3

			}

		}
		add_core_of = MAL
		1942.11.22 = {
			owner = ENG
			controller = JAP

		}
		1946.1.1 = {
			owner = ENG
			controller = ENG

		}
		1972.1.1 = {
			owner = MAL
			controller = MAL

		}

	}

	provinces={
		12299 
	}
	manpower=1022096
	buildings_max_level_factor=1.000
	state_category=town
}

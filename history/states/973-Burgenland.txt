
state={
	id=973
	name="STATE_973"

	history={
		owner = AUS
		victory_points = {
			9661 2 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1

		}
		add_core_of = AUS
		1938.3.12 = {
			owner = GER
			controller = GER
			add_core_of = GER

		}
		1946.1.1 = {
			owner = RUS
			remove_core_of = GER

		}
		1956.1.1 = {
			owner = AUS

		}

	}

	provinces={
		3700 9661 11630 13632 
	}
	manpower=284900
	buildings_max_level_factor=1.000
	state_category=rural
}

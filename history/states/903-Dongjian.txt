state= {
	id=903
	name="STATE_903"
	manpower = 684600
	state_category = town
	resources={
		tungsten=1
	}

	history= {
		add_core_of = CHI
		owner = PRC
		add_core_of = GND
		add_core_of = PRC		
		1936.1.1 = {
			owner = GND
			add_core_of = GND
			add_core_of = PRC
		}
		1937.1.1 = {
			owner = CHI
			controller = CHI
		}	
		1946.1.1 = {
			owner = CHI
			controller = CHI
		}	
		1950.1.1 = {
			owner = PRC
		}	
		buildings = {
			infrastructure = 2
		}				
	}
	provinces={
		1078 7141
	}
}
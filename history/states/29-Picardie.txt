
state={
	id=29
	name="STATE_29"
	resources={
		steel=56.000
		aluminium=11.000
	}

	history={
		owner = FRA
		victory_points = {
			11420 2 
		}
		victory_points = {
			9561 1 
		}
		buildings = {
			infrastructure = 8
			industrial_complex = 2
			air_base = 2

		}
		add_core_of = FRA
		1939.1.1 = {
			buildings = {
				industrial_complex = 5
				infrastructure = 8

			}

		}
		1941.6.22 = {
			owner = FRA
			controller = GER
			add_core_of = VIC

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		454 6585 9561 11420 11549 
	}
	manpower=910108
	buildings_max_level_factor=1.000
	state_category=town
}


state={
	id=104
	name="STATE_104"
	resources={
		aluminium=7.000
		steel=5.000
	}

	history={
		owner = BOS
		victory_points = {
			11899 5 
		}
		victory_points = {
			6799 2 
		}
		victory_points = {
			13602 1
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			air_base = 2

		}
		add_core_of = BOS
		add_claim_by = OTT
		1909.1.1 = {
			owner = AUS
		}

	}

	provinces={
		3985 6614 6799 6957 9586 9922 11899 13602 
	}
	manpower=2457400
	buildings_max_level_factor=1.000
	state_category=town
}

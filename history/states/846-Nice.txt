
state={
	id=846
	name="STATE_846"

	history={
		owner = FRA
		add_core_of = FRA
		victory_points = {
			9909 5 
		}
		buildings = {
			infrastructure = 5
			9909 = {
				naval_base = 2

			}

		}
		1941.6.22 = {
			owner = FRA
			controller = ITA
			add_core_of = VIC

		}
		1944.6.20 = {
			owner = FRA
			controller = GER

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		3959 9909 
	}
	manpower=510700
	buildings_max_level_factor=1.000
	state_category=town
}

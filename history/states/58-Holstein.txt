
state={
	id=58
	name="STATE_58"

	history={
		owner = GER
		victory_points = {
			6389 10 
		}
		victory_points = {
			11331 3 
		}
		victory_points = {
			13763 1 
		}
		buildings = {
			infrastructure = 5
			arms_factory = 1
			dockyard = 3
			air_base = 5
			6389 = {
				naval_base = 8

			}

		}
		add_core_of = GER
		1939.1.1 = {
			buildings = {
				synthetic_refinery = 3
				anti_air_building = 5
				dockyard = 7
				arms_factory = 4

			}

		}
		1946.1.1 = {
			owner = ENG
			controller = ENG

		}
		1950.1.1 = {
			owner = FRG

		}

	}

	provinces={
		3231 3368 6257 6389 9320 11331 13763 
	}
	manpower=3978150
	buildings_max_level_factor=1.000
	state_category=town
}

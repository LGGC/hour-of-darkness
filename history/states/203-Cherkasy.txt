
state={
	id=203
	name="STATE_203"

	history={
		owner = RUS
		victory_points = {
			488 2
		}
		buildings = {
			infrastructure = 5

		}
		add_core_of = RUS
		add_core_of = UKR
		1942.11.21 = {
			owner = RUK

		}
		1944.6.20 = {
			controller = RUS

		}
		1946.1.1 = {
			owner = RUS

		}
		2000.1.1 = {
			owner = UKR
			controller = UKR
			remove_core_of = RUS

		}

	}

	provinces={
		458 488 9451 11424 11438 11454 
	}
	manpower=2405190
	buildings_max_level_factor=1.000
	state_category=large_town
}


state={
	id=594
	name="STATE_594"
	resources={
		tungsten=11.000
		steel=8.000
		chromium=3.000
	}

	history={
		owner = GXC
		add_core_of = GXC
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 3
			industrial_complex = 1

		}
		victory_points = {
			7137 10 
		}
		1950.1.1 = {
			owner = PRC
		}

	}

	provinces={
		1087 1131 4121 4177 7044 7137 10050 10105 11983 12023 
	}
	manpower=6550000
	buildings_max_level_factor=1.000
	state_category=town
}


state={
	id=97
	name="STATE_97"

	history={
		owner = RUS
		victory_points = {
			11301 2 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			air_base = 3

		}
		add_core_of = POL
		1940.5.10 = {
			owner = RUS

		}
		1942.11.21 = {
			controller = GER

		}
		1945.1.1 = {
			controller = RUS

		}
		1946.1.1 = {
			owner = POL
			controller = POL

		}

	}

	provinces={
		290 347 400 11247 11301 11357 
	}
	manpower=1461010
	buildings_max_level_factor=1.000
	state_category=rural
}

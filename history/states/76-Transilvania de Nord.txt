
state={
	id=76
	name="STATE_76"

	resources = {
			steel = 9
			oil = 2
	}

	history={
		owner = AUS
		victory_points = {
			6711 5 
		}
		victory_points = {
			3696 2
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1

		}
		add_core_of = ROM
		add_core_of = AUS
		add_core_of = HUN
		1941.6.22 = {
			owner = HUN
			controller = HUN
		}
		1945.1.1 = {
			controller = RUS
		}
		1946.1.1 = {
			owner = ROM
			controller = ROM
		}
	}

	provinces={
		713 727 3696 3709 6711 6714 6731 9672 9687 11676 
	}
	manpower=1417301
	buildings_max_level_factor=1.000
	state_category=rural
}

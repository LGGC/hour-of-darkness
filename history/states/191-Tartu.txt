
state={
	id=191
	name="STATE_191"

	history={
		owner = RUS
		victory_points = {
			9221 3 
		}
		victory_points = {
			308 2 
		}
		victory_points = {
			11127 2 
		}
		buildings = {
			infrastructure = 5

		}
		add_core_of = EST
		1941.6.21 = {
			owner = RUS
			controller = RUS
		}
		1942.11.21 = {
			owner = ROS
			controller = ROS
		}
		1944.6.20 = {
			owner = RUS
			controller = GER
		}
		1945.1.1 = {
			owner = RUS
			controller = RUS
		}
		1946.1.1 = {
			add_core_of = RUS
		}
		2000.1.1 = {
			owner = EST
			remove_core_of = RUS
		}
	}

	provinces={
		308 592 6099 6178 6381 6408 9079 9221 9485 11057 11127 11443 13127 13491 
	}
	manpower=108973
	buildings_max_level_factor=1.000
	state_category=rural
}


state={
	id=772
	name="STATE_772"

	history={
		owner = AUS
		victory_points = {
			11787 1 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 1
			industrial_complex = 1

		}
		add_core_of = AUS
		add_core_of = HUN
		add_core_of = SER
		add_core_of = ROM
		1941.6.22 = {
			controller = GER

		}
		1945.1.1 = {
			controller = YUG

		}

	}

	provinces={
		614 3614 6643 11787 
	}
	manpower=597952
	buildings_max_level_factor=1.000
	state_category=large_town
}


state={
	id=168
	name="STATE_168"

	resources = {
		steel = 8
		aluminium =4
	}

	history={
		owner = SPR
		victory_points = {
			1093 5
		}
		victory_points = {
			11884 2
		}
		victory_points = {
			10024 2
		}
		buildings = {
			infrastructure = 5
			dockyard = 1
			air_base = 2
			10024 = {
				naval_base = 8

			}

		}
		add_core_of = SPR
		add_core_of = SPA
		1939.9.1 = {
			owner = SPA
		}
	}

	provinces={
		832 1093 3835 7111 9903 10024 10109 11807 11884 
	}
	manpower=1007605
	buildings_max_level_factor=1.000
	state_category=town
}

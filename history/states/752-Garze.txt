
state={
	id=752
	name="STATE_752"

	history={
		buildings = {
			infrastructure = 1

		}
		victory_points = {
			12724 1 
		}
			owner = SIC
			add_core_of = XIK
			add_core_of = CHI
			add_core_of = PRC	

		1939.1.1 = {
			buildings = {
				industrial_complex = 1
				infrastructure = 2

			}

		}
		1950.1.1 = {
			owner = CHI
			remove_core_of = XIK

		}
		1953.1.1 = {
			owner = PRC

		}

	}

	provinces={
		1999 8104 12724 12837 
	}
	manpower=360000
	buildings_max_level_factor=1.000
	state_category=pastoral
}

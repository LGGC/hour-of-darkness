﻿capital = 335 #Jakarta
oob = "GEN_GARRISON" # "INS_1908"
set_convoys = 25
set_research_slots = 2
#######################
# Research
#######################
set_technology = {
	Small_Arms_1918 = 1
	tech_support = 1		
	tech_recon = 1
	Fighter_1933 = 1
	Tactical_Bomber_1933 = 1
	transport = 1

	Tank_1916 = 1
	Tank_1918 = 1
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = democratic
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no	
}

set_popularities = {
    fascist = 0
    authoritarian = 27
    democratic = 53
    socialist = 20
    communist = 0
}
set_cosmetic_tag = INS_HOL # Netherlands East Indies
#######################
# Leaders
#######################
# Democracy
create_country_leader = {
	name = "A. T. van Starkenborgh Stachouwer"
	desc = ""
	picture = "P_D_At_Van_Starkenborgh.tga"
	expire = "1953.3.1"
	ideology = social_conservatism 
	traits = {}
}
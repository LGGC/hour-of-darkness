﻿#########################################################################
# Mengjiang
#########################################################################
capital = 827
oob = "GEN_GARRISON" # "GEN_GARRISON"
set_convoys = 0
#######################
# Research
#######################
set_technology = {
	Small_Arms_1918 = 1
	gw_artillery = 1
}

add_ideas = {
	MEN_Nomadic_Lifestyle
}

set_politics = {	
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}

create_country_leader = {
	name = "Demchugdongrub"
	desc = ""
	picture = "P_A_Demchugdongrub.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {
		spirit_of_genghis
	}
}

create_country_leader = {
	name = "Demchugdongrub"
	desc = ""
	picture = "P_A_Demchugdongrub.tga"
	expire = "1965.1.1"
	ideology = fascism
	traits = {
		spirit_of_genghis
	}
}

create_corps_commander = {
	name = "Namnansüren"
	picture = "P_A_De_Noyan.tga"
	traits = { }
	skill = 1
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 1
}

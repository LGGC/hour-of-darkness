﻿capital = 542

oob = "GEN_GARRISON" # "BOT_1908"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	tech_mountaineers = 1
}

set_convoys = 5


set_politics = {	
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}

create_country_leader = {
	name = "Tshekedi Khama"
	picture = "Portrait_Africa_Generic_2.dds"
	expire = "1965.1.1"
	ideology = trotskyism
	traits = {
		#
	}
}
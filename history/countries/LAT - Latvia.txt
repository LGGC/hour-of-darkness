﻿capital = 12

oob = "GEN_GARRISON" # "GEN_GARRISON"

add_ideas = {
	two_year_service
}

set_technology = {
	Small_Arms_1918 = 1
	Small_Arms_1936 = 1
	Fighter_1933 = 1
#Naval Stuff
	SS_1895 = 1
	SS_1912 = 1
#
}

set_research_slots = 3

set_convoys = 10

set_politics = {	
	ruling_party = democratic
	last_election = "1931.10.3"
	election_frequency = 108
	elections_allowed = no
}

set_popularities = {
    fascist = 1
    authoritarian = 2
    democratic = 86
    socialist = 5
    communist = 6
}
####################################################
# Latvian Leaders
####################################################
# Paternal Autocracy
create_country_leader = {
	name = "Karlis Ulmanis"
	desc = ""
	picture = "P_A_Karlis_Ulmanis.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
# democraticism
create_country_leader = {
	name = "Alberts Kviesis"
	desc = ""
	picture = "P_L_Alberts_Kviesis.tga"
	expire = "1965.1.1"
	ideology = social_liberalism
	traits = {}
}
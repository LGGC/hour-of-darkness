﻿#########################################################################
# Syria
#########################################################################
capital = 554
set_convoys = 5
#######################
# Research
#######################
set_technology = {
	Small_Arms_1918 = 1
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = democratic
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 1
    democratic = 74
    socialist = 25
    communist = 0
}
#######################
# Leaders
#######################
# Fascism
create_country_leader = {
	name = "Antun Saadeh"
	desc = ""
	picture = "P_F_Antun_Saadeh.tga"
	ideology = national_socialism
	traits = {}
}
# Democracy
create_country_leader = {
	name = "Hashim al-Atassi"
	desc = ""
	picture = "P_D_Hashim_AlAtassi.tga"
	ideology = social_liberalism
	traits = {}
}
# Socialism
create_country_leader = {
	name = "Michel Aflaq"
	desc = ""
	picture = "P_S_Michel_Aflaq.tga"
	ideology = arab_socialism
	traits = {}
}
# Communism
create_country_leader = {
	name = "Khalid Bakdash"
	desc = ""
	picture = "P_C_Khalid_Bakdash.tga"
	ideology = marxism_leninism
	traits = {}
}
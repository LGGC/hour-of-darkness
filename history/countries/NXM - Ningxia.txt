﻿capital = 283

oob = "GEN_GARRISON" # "NXM_1914"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	Mass_Assault = 1
}
set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}

create_country_leader = {
	name = "Ma Hung-k'uei"
	desc = ""
	picture = "Xibei_PA_Ma_Hongkui.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}

create_field_marshal = {
	name = "Ma Hung-k'uei"
	picture = "Xibei_PA_Ma_Hongkui.tga"
	traits = { offensive_doctrine trait_reckless }
	skill = 4
	attack_skill = 4
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 1
}
create_field_marshal = {
	name = "Ma Hungpin"
	picture = "P_A_Ma_Hongbin.tga"
	traits = { defensive_doctrine } 
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}
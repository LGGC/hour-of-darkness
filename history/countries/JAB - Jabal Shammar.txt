﻿#########################################################################
# Jabal Shammar - 1908
#########################################################################
1908.1.1 = {
capital = 292
set_stability = 0.6
set_war_support = 0.3
oob = "JAB_1908"
set_research_slots = 1
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits

# Laws & Policies

}
#######################
# Research
#######################
set_technology = {
}	
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1908.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
####################################################
# Leaders
####################################################
# Autocracy
create_country_leader = {
	name = "Saud bin Abdulaziz"
		desc = "Saud_bin_Abdulaziz_desc"
		picture = "P_A_Saud_bin_Abdulaziz.tga"
		expire = "1920.1.1"
		ideology = monarchism
		traits = {
			POSITION_Emir
			ideology_A 
			SUBIDEOLOGY_Monarchism
		}
}
}
#########################################################################
# Jabal Shammar - 1914
#########################################################################
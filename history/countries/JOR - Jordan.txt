﻿capital = 455

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	
}

set_convoys = 5


set_politics = {	
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    fascist = 0
    authoritarian = 55
    democratic = 40
    democratic = 2
    democratic = 3
    socialist = 0
    communist = 0
    communist = 0
}

create_country_leader = {
	name = "Hashim Kheir"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_2.dds"
	ideology = social_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Saeed Ahmadi"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_fascist1.dds"
	ideology = fascism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_land_3.dds"
	ideology = authoritarian_democracy
	traits = {
		#
	}
}

create_country_leader = {
	name = "Fu'ad Nassar"
	desc = ""
	picture = "gfx/leaders/JOR/Portrait_Arabia_Generic_navy_2.dds"
	ideology = trotskyism
	traits = {
		#
	}
}
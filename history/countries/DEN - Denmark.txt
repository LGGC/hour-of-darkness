﻿#########################################################################
# Kingdom of Denmark - 1908
#########################################################################
1908.1.1 = {
capital = 37
set_stability = 0.7
set_war_support = 0.7
oob = "GEN_GARRISON" # "DEN_1908"
set_research_slots = 3
set_convoys = 50
#######################
# Diplomacy
#######################
add_ideas = {
	DEN_HoG_Jens_Christensen
	DEN_FM_Frederik_RabenLevetzau
	DEN_MoS_Sigurd_Berg
	DEN_AM_Vilhelm_Lassen
	DEN_CoArmy_Vilhelm_Gortz
# National Spirits

# Laws & Policies

}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1906.1.29"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 52
    democratic = 48
    socialist = 0
    communist = 0
}
####################################################
# Leaders
####################################################
# Democratic
	create_country_leader = {
		name = "Frederick VIII"
		desc = "Frederick_VIII_desc"
		picture = "gfx/leaders/DEN/Frederick_VIII.dds"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { POSITION_King SUBIDEOLOGY_Authoritarian_Democracy }
	}
}
#########################################################################
# Kingdom of Denmark - 1914
#########################################################################
1914.1.1 = {
capital = 37
set_stability = 0.7
set_war_support = 0.7
oob = "GEN_GARRISON" # "DEN_1914"
set_research_slots = 3
set_convoys = 50
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits

# Laws & Policies

}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1912.6.14"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 67
    democratic = 33
    socialist = 0
    communist = 0
}
####################################################
# Leaders
####################################################
# Democratic
create_country_leader = {
	name = "Christian X"
		desc = ""
		picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"	#NO PORTRAIT YET
		expire = "1965.1.1"
		ideology = monarchism
		traits = {}
	}
}
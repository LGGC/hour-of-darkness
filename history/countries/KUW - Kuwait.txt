﻿#########################################################################
# Kuwait - 1908
#########################################################################
1908.1.1 = {
	capital = 656
	set_stability = 0.5
	set_war_support = 0.3
	set_research_slots = 1
	#######################
	# Research
	#######################
	set_technology = {
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1908.1.1"
		election_frequency = 48
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 100
		democratic = 0
		socialist = 0
		communist = 0
	}
	#######################
	# Leaders
	#######################
	# Authoritarianism
	create_country_leader = {
		name = "Mubarak Al-Sabah"
		desc = "Mubarak_Al_Sabah_desc"
		picture = "P_A_Mubarak_Al_Sabah.tga"
		expire = "1915.11.28"
		ideology = monarchism
		traits = { 
			POSITION_Sheikh 
			ideology_A 
			SUBIDEOLOGY_Monarchism 
		}
	}
}
	#######################
	# Generals
	#######################


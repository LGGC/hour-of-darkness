﻿#########################################################################
# Hunan
#########################################################################
capital = 602
set_stability = 0.1
set_war_support = 0.2
oob = "GEN_GARRISON" # "HNN_1933"
set_convoys = 5
#######################
# Research
#######################
set_technology = {
	Small_Arms_1918 = 1
	mass_assault = 1
#Naval Stuff
	DD_1885 = 1
	
	CL_1885 = 1
	CL_1900 = 1
	CL_1912 = 1
	CL_1922 = 1
	
	BB_1885 = 1
	BB_1895 = 1
	
	CA_1885 = 1
	CA_1895 = 1
#
}
#######################
# Politics
#######################
set_politics = {
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Ho Chien"
	desc = "POLITICS_LI_ZONGREN_DESC"
	picture = "P_A_He_Jian.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Ho Chien"
	picture = "P_A_He_Jian.tga"
	traits = { defensive_doctrine trait_cautious politically_connected }
	skill = 3
	attack_skill = 1
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 3
}
﻿#########################################################################
# Chahar Army / That one warlord
#########################################################################
1908.1.1 = {
	capital = 608
	set_stability = 0.6
	set_war_support = 0.6
	oob = "GEN_GARRISON" # "AJC_1914"
	set_convoys = 5
	#######################
	# Research
	#######################
	set_technology = {

	#
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
}

    set_popularities = {
        fascist = 0
        authoritarian = 100
        democratic = 0
        socialist = 0
        communist = 0
    }
	#######################
	# Leaders
	#######################
	# Paternal Autocracy
	create_country_leader = {
		name = "Feng Yu-hsiang"
		desc = "Feng_Yu-hsiang_desc"
		picture = "P_A_Feng_Yu-hsiang.tga"
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = {}
	}
	
# Communism
create_country_leader = {
	name = "Yang Chingyu"
	desc = "Jingyu_desc"
	picture = "P_A_Yang_Chingyu.tga"
	expire = "1965.1.1"
	ideology = maoism
	traits = { cornered_fox }
}


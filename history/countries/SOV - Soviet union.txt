﻿#########################################################################
# Soviet Union
#########################################################################
capital = 219
set_stability = 0.6
set_war_support = 0.8
oob = "GEN_GARRISON" # "SOV_1917"
set_research_slots = 4
set_convoys = 30
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = communist
	last_election = "1914.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 2
    democratic = 8
    socialist = 10
    communist = 80
}
add_ideas = {
# National Spirits
	home_of_revolution
	SOV_Soviet_Economics	
# Cabinet
	SOV_HoG_Vyacheslav_M_Molotov
	SOV_FM_Maksim_M_Litvinov
	SOV_AM_Lazar_M_Kaganovich
	SOV_MoS_Genrikh_G_Yagoda
	SOV_HoI_Yan_K_Berzin
# Military Staff
	SOV_CoStaff_Mikhail_N_Tukhatchevskiy
	SOV_CoArmy_Kliment_Y_Voroshilov
	SOV_CoNavy_Vladimir_M_Orlov
	SOV_CoAir_Yakov_I_Alksnis	
}
#######################
# Diplomacy
#######################
create_faction = Red_Movement

declare_war_on = {
	target = RUS
	type = civil_war
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Kirill Vladimirovich"
	desc = ""
	picture = "P_A_Kirill_Vladimirovich.tga"
	expire = "1938.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
# Democratic
create_country_leader = {
	name = "Pavel Milyukov"
	desc = ""
	picture = "P_D_Pavel_Milyukov.tga"
	expire = "1965.1.1"
	ideology = social_liberalism
	traits = {}
}
# Socialism
create_country_leader = {
	name = "Viktor Chernov"
	desc = ""
	picture = "P_S_Viktor_Chernov.tga"
	expire = "1965.1.1"
	ideology = socialism
	traits = {}
}
# Communism
create_country_leader = {
	name = "Vladimir Lenin"
	desc = "Vladimir_Lenin_desc"
	picture = "P_C_Vladimir_Lenin.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {}
}
#######################
# Generals
#######################

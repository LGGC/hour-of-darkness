﻿1908.1.1 = {
	capital = 293

	oob = "YEM_1908"

	# Starting tech
	set_technology = {
	}
	set_research_slots = 1
	set_convoys = 5
	set_politics = {
		ruling_party = authoritarian
		last_election = "1908.1.1"
		election_frequency = 48
		elections_allowed = no
	}

    set_popularities = {
        fascist = 0
        authoritarian = 100
        democratic = 0
        socialist = 0
        communist = 0
    }
	####################################################
	# Yemeni Leaders
	####################################################
	# Paternal Autocracy
	create_country_leader = {
		name = "Yahya Muhammad Hamid ed-Din"
		desc = "Yahya_Muhammad_Hamid_desc"
		picture = "P_Arabia_10.tga" # Generic so there are no pics of him
		expire = "1948.2.17"
		ideology = monarchism
		traits = {
			POSITION_Imam
			ideology_A 
			SUBIDEOLOGY_Monarchism
		}
	}
}
﻿#########################################################################
# Kingdom of Spain - 1908
#########################################################################
1908.1.1 = {
capital = 41
oob = "GEN_GARRISON" # "SPR_1908"
set_research_slots = 3
set_stability = 0.5
set_war_support = 0.7
set_convoys = 200
#######################
# Diplomacy
#######################
add_ideas = {
# Cabinet
	SPR_HoG_Alfonso_XIII
# Military Staff
	
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = democratic
	last_election = "1886.1.1"
	election_frequency = 36
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 5
    democratic = 95
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Alfonso XIII"
	desc = ""
	picture = "Alfonso_XIII.dds"
	ideology = monarchism
	traits = { POSITION_King SUBIDEOLOGY_Authoritarian_Democracy }
}
create_country_leader = {
	name = "Antonio Maura"
	desc = ""
	picture = "Antonio_Maura.dds"
	ideology = social_conservatism
	traits = { POSITION_Prime_Minister SUBIDEOLOGY_Conservatism }
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Rojo Lluch"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = { logistics_wizard }
	skill = 2
}

create_corps_commander = {
	name = "Asensio Torrado"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = {}
	skill = 2
    attack_skill = 1
    defense_skill = 3
    planning_skill = 2
    logistics_skill = 1	
}

create_field_marshal = {
	name = "Martínez Cabrera"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = { logistics_wizard }
	skill = 1
}

create_field_marshal = {
	name = "Miaja Menant"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = { logistics_wizard defensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Casado López"
		portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
		traits = { offensive_doctrine }
		skill = 1
	}
}
#########################################################################
# Kingdom of Spain - 1914
#########################################################################
1914.1.1 = {
capital = 41
oob = "GEN_GARRISON" # "SPR_1914"
set_research_slots = 3
set_stability = 0.5
set_war_support = 0.7
set_convoys = 200
#######################
# Diplomacy
#######################
add_ideas = {
# Cabinet
	SPR_HoG_Manuel_Portela_Valladares
	SPR_FM_Joaquin_Urzaiz_Cadaval
	SPR_AM_Jose_Mendizabal_y_Bonilla
	SPR_MoS_Manuel_Portela_Valladares
	SPR_HoI_Juan_Hernandez_Sarabia
# Military Staff
	SPR_CoStaff_Jose_Miaja_Menant
	SPR_CoArmy_Nicolas_Molero_Lobo
	SPR_CoNavy_Antonio_Azarola_Gresillon
	SPR_CoAir_Miguel_Nunez_de_Prado	
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1886.1.1"
	election_frequency = 36
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Alfonso XIII"
	desc = ""
	picture = "P_A_Alfonso_XIII.png"
	expire = "1965.1.1"
	ideology = monarchism
	traits = {}
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Rojo Lluch"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = { logistics_wizard }
	skill = 2
}

create_corps_commander = {
	name = "Asensio Torrado"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = {}
	skill = 2
    attack_skill = 1
    defense_skill = 3
    planning_skill = 2
    logistics_skill = 1	
}

create_field_marshal = {
	name = "Martínez Cabrera"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = { logistics_wizard }
	skill = 1
}

create_field_marshal = {
	name = "Miaja Menant"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
	traits = { logistics_wizard defensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Casado López"
		portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_2.dds"
		traits = { offensive_doctrine }
		skill = 1
	}
}
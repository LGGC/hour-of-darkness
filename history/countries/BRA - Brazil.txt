﻿#########################################################################
# Brazil - 1908
#########################################################################
1908.1.1 = {
capital = 500
set_stability = 0.5
set_war_support = 0.8
oob = "GEN_GARRISON" # "BRA_1908"
set_research_slots = 2
set_convoys = 40
#######################
# Diplomacy
#######################
add_ideas = {
# Cabinet
	BRA_HoG_Getulio_Vargas
	BRA_FM_Jose_de_Macedo_Soares
	BRA_AM_Joao_Cafe_Filho
	BRA_MoS_Marcondes_Filho
	BRA_HoI_Filinto_Muller
# Military Staff
	BRA_CoStaff_Eurico_Gaspar_Dutra
	BRA_CoArmy_Joao_Neves_da_Fontoura
	BRA_CoNavy_Aristides_Guilhem
	BRA_CoAir_Salgado_Filho
}

set_country_flag = monroe_doctrine
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = democratic
	last_election = "1906.3.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 9
    democratic = 91
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Democracy
create_country_leader = {
	name = "Venceslau Brás"
	desc = ""
	picture = "P_D_Venceslau_Bras.png"
	expire = "1965.1.1"
	ideology = social_liberalism
	traits = {}
}
create_country_leader = {
	name = "Hermes da Fonseca"
	desc = ""
	picture = "P_D_Hermes_da_Fonseca.png"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}
create_country_leader = {
	name = "Afonso Pena"
	desc = ""
	picture = "P_D_Hermes_da_Fonseca.png"	#NO PORTRAIT YET
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}
#######################
# Generals
#######################
create_corps_commander = {
	name = "Eurico Gaspar Dutra"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_1.dds"
	traits = { ranger }
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Mascarenhas de Morais"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_5.dds"
	traits = { jungle_rat }
	skill = 4
	attack_skill = 2
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 4
}
#######################
# Admirals
#######################
create_navy_leader = {
	name = "Augusto Rademaker"
		portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_navy_3.dds"
		traits = {  }
		skill = 3
	}
}
#########################################################################
# Brazil - 1914
#########################################################################
1914.1.1 = {
capital = 500
set_stability = 0.5
set_war_support = 0.8
oob = "GEN_GARRISON" # "BRA_1914"
set_research_slots = 2
set_convoys = 40
#######################
# Diplomacy
#######################
add_ideas = {
# Cabinet
	BRA_HoG_Getulio_Vargas
	BRA_FM_Jose_de_Macedo_Soares
	BRA_AM_Joao_Cafe_Filho
	BRA_MoS_Marcondes_Filho
	BRA_HoI_Filinto_Muller
# Military Staff
	BRA_CoStaff_Eurico_Gaspar_Dutra
	BRA_CoArmy_Joao_Neves_da_Fontoura
	BRA_CoNavy_Aristides_Guilhem
	BRA_CoAir_Salgado_Filho
}

set_country_flag = monroe_doctrine
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = democratic
	last_election = "1910.3.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 9
    democratic = 91
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Democracy
create_country_leader = {
	name = "Venceslau Brás"
	desc = ""
	picture = "P_D_Venceslau_Bras.png"
	expire = "1965.1.1"
	ideology = social_liberalism
	traits = {}
}

create_country_leader = {
	name = "Hermes da Fonseca"
	desc = ""
	picture = "P_D_Hermes_da_Fonseca.png"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}
#######################
# Generals
#######################
create_corps_commander = {
	name = "Eurico Gaspar Dutra"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_1.dds"
	traits = { ranger }
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Mascarenhas de Morais"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_5.dds"
	traits = { jungle_rat }
	skill = 4
	attack_skill = 2
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 4
}
#######################
# Admirals
#######################
create_navy_leader = {
	name = "Augusto Rademaker"
		portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_navy_3.dds"
		traits = {  }
		skill = 3
	}
}
﻿capital = 294

oob = "GEN_GARRISON" # "OMA_1914"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
}

set_convoys = 10


set_politics = {	
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    fascist = 0
    authoritarian = 78
    democratic = 15
    democratic = 4
    democratic = 0
    socialist = 3
    communist = 0
    communist = 0
}

1939.1.1 = {
	set_politics = {	
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
}

    set_popularities = {
        fascist = 0
        authoritarian = 78
        democratic = 15
        socialist = 3
        communist = 0
    }
}
####################################################
# Omani Leaders
####################################################
# Paternal Autocracy
create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "P_A_Said_bin_Taimur.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
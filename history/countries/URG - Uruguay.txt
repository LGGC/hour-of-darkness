﻿
capital = 300
set_country_flag = monroe_doctrine
set_convoys = 5
oob = "GEN_GARRISON" # "URG_1914"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	gw_artillery = 1
	Fighter_1933 = 1
# Naval Tech
	DD_1885 = 1
}


set_politics = {	
	ruling_party = democratic
	last_election = "1934.4.19"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 0
    democratic = 90
    socialist = 10
    communist = 0
}

create_country_leader = {
	name = "Gabriel Terra"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "P_D_Gabriel Terra.tga"
	expire = "1965.1.1"
	ideology = social_liberalism
	traits = {}
}

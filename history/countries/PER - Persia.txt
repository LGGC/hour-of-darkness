﻿#########################################################################
# Persia
#########################################################################
capital = 266
oob = "GEN_GARRISON" # "PER_1914"
set_convoys = 10
#######################
# Research
#######################
# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	Small_Arms_1936 = 1
	gw_artillery = 1
	Fighter_1933 = 1
	CAS_1936 = 1
}
#######################
# Research
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 63
    democratic = 35
    socialist = 2
    communist = 0
}
#######################
# Leaders
#######################
# Fascism
create_country_leader = {
	name = "Davud Monshizadeh"
	desc = ""
	picture = "P_F_Davud_Monshizadeh.tga"
	expire = "1965.1.1"
	ideology = national_socialism
	traits = {}
}
# Autocracy
create_country_leader = {
	name = "Reza Shah Pahlavi"
	desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
	picture = "P_A_Reza_Shah_Pahlavi.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = { freind_of_germany }
}
# Democracy
create_country_leader = {
	name = "Mohammad Mosaddegh"
	desc = ""
	picture = "P_D_Mohammad_Mosaddegh.tga"
	expire = "1965.1.1"
	ideology = social_democracy
	traits = {}
}
# Socialism
create_country_leader = {
	name = "Mozzafar Baghai"
	desc = ""
	picture = "P_S_Mozzafar_Baghai.tga"
	expire = "1965.1.1"
	ideology = democratic_socialism
	traits = {}
}
# Communism
create_country_leader = {
	name = "Iraj Eskandari"
	desc = ""
	picture = "P_C_Iraj_Eskandari.tga"
	expire = "1965.1.1"
	ideology = marxism_leninism
	traits = {}
}

#######################
# Generals
#######################
create_corps_commander = {
	name = "Hasan Arfa"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_5.dds"
	traits = {  desert_fox }
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}
﻿#########################################################################
# Principality of Samos
#########################################################################
############################################
############################################
1908.1.1 = {

capital = 1119
set_stability = 0.6
set_war_support = 0.1
set_research_slots = 1
oob = "SAM_1908"

set_cosmetic_tag = SAM_PRINC
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1908.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
add_ideas = {

# Spirits

# Cabinet

# Military Staff

}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Andreas Kopasis" # The Prince of Samos
	desc = "Andreas_Kopasis_desc"
	picture = "P_A_Andreas_Kopasis.tga"
	expire = "1965.1.1"
	ideology = monarchism
	traits = {}
}

}
#######################
# Generals
#######################

﻿#########################################################################
# Kingdom of Montenegro - 1908
#########################################################################
1908.1.1 = {
capital = 105 # Cetinje
oob = "GEN_GARRISON" # "GEN_GARRISON"
set_research_slots = 2
set_war_support = 0.4
set_stability = 0.5
set_convoys = 10
#######################
# Diplomacy
#######################
add_ideas = {
# Laws and Policies

# Cabinet

# Military Staff

}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1860.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
set_cosmetic_tag = MNT_PRINC
#######################
# Leaders
#######################
create_country_leader = {
	name = "Nicholas I"
		desc = ""
		picture = "Nicholas_I.png"
		expire = "1960.1.1"
		ideology = monarchism
		traits = { }
	}
}
#########################################################################
# Kingdom of Montenegro - 1914
#########################################################################
1914.1.1 = {
capital = 105 # Cetinje
oob = "GEN_GARRISON" # "MNT_1914"
set_research_slots = 3
set_war_support = 0.7
set_stability = 0.6
set_convoys = 10
#######################
# Diplomacy
#######################
add_ideas = {
# Laws and Policies

# Cabinet

# Military Staff

}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1860.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
create_country_leader = {
	name = "Nicholas I"
		desc = ""
		picture = "Nicholas_I.png"
		expire = "1960.1.1"
		ideology = monarchism
		traits = { }
	}
}
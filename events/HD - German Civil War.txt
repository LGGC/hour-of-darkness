﻿###########################
# Hour of Darkness German Civil War Events
###########################
add_namespace = HD_German_CivWar
#########################################################################
#  Outbreak of the Civil War
#########################################################################

country_event = {
	id = HD_German_CivWar.1
	title = HD_German_CivWar.1.t
	desc = HD_German_CivWar.1.d
	picture = GFX_news_ENG_London_Fire
	is_triggered_only = yes
	
	#
	option = {
		name = HD_German_CivWar.1.a
		country_event = { id = HD_German_CivWar.2 days = 3 }
		GER = { # Tirpitz	
			set_politics = {
				ruling_party = fascist
				elections_allowed = no
			}
			
			hidden_effect = {
				set_province_controller = 3535 # Leipzig Area
				set_province_controller = 9471
				set_province_controller = 9411	
				set_province_controller = 6594 # Franken Area
				set_province_controller = 11417
				set_capital = 808
				set_cosmetic_tag = GER_FTL_Tirpitz
				create_country_leader = {
					name = "Alfred von Tirpitz"
					desc = ""
					picture = "M_Alfred_von_Tirpitz.tga"
					ideology = ultranationalism
					traits = {
						POSITION_Grand_Admiral
						SUBIDEOLOGY_Ultranationalism
					}
				}
				remove_ideas = GER_HoG_Bernhard_Von_Bulow
				add_ideas = GER_HoG_Wolfgang_Kapp
			}
			set_cosmetic_tag = GER_FATHERLAND
			start_civil_war = {
				ruling_party = fascist
				ideology = authoritarian
				size = 0.6
				air_ratio = 0.5
				capital = 52 # Frankfurt
				states = {
					28 # Alsace
					807 # Baden
					806 # Saar
					42 # Moselland
					802 # Eupen Malmedy something
					51 # Rhinelnad
					50 # Wurtemmburg
					52 # Oberbayern 
					809 # East Rhine
					57 # Westfalen
					54 # Franken
					59 # Hannover 
					61 # Mecklenburg
					53 # Niederbayern
					65 # Sachsen
					68 # Ostmark
					63 # Hinterpommern
					86 # Posen
					768 # South East Prussia
					798 # Gdansk
					766 # Eastern Silesia
					66 # Niedershclif
					67 # Oberschlifj
				}
			}	
		}
		random_other_country = {
			limit = {
				original_tag = GER
				has_government = authoritarian
			} 
			set_stability = 0.9
			set_war_support = 0.8
			hidden_effect = {
				set_province_controller = 9388 # Stettin
				set_province_controller = 11316
				set_province_controller = 11372
				set_province_controller = 9306

				set_province_controller = 3234 # 
				set_province_controller = 247
				set_province_controller = 11388
				set_province_controller = 9281
				set_province_controller = 11233

				set_province_controller = 3384 # Konigsberg
				set_province_controller = 281
				set_province_controller = 348

				set_province_controller = 11359 # Brandenburg
				set_province_controller = 9238
				set_province_controller = 9375
				set_province_controller = 11468
				set_province_controller = 11545
				set_province_controller = 3572
				set_province_controller = 3367
				set_province_controller = 9535
				set_province_controller = 3572
				set_province_controller = 478
				set_province_controller = 6441
				set_province_controller = 9496

				set_province_controller = 6501 # Thuringen
				set_province_controller = 9497
				set_province_controller = 6582
				set_province_controller = 9411
				set_province_controller = 3561
				set_province_controller = 9411
				set_province_controller = 3500
				remove_ideas = GER_HoG_Bernhard_Von_Bulow
				add_ideas = GER_HoG_Moltke_The_Younger
			}
		}
	}
}

country_event = {
	id = HD_German_CivWar.2
	title = HD_German_CivWar.2.t
	desc = HD_German_CivWar.2.d
	picture = GFX_news_ENG_London_Fire
	hidden = yes
	is_triggered_only = yes

	option = {
		name = HD_German_CivWar.2.a
        set_global_flag = Super_Event_Visible
        set_global_flag = Spevent_German_CivWar
	}
}

#58
#56
#804
#62
#85
#64
#66 # Niederschlseion
#67 # Oberschliseffejdjghj
#766 # Eastern Silesia
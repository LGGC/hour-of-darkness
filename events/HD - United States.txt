﻿###########################
# Hearts of Darkness Events : United States
###########################
add_namespace = HD_USA_Political
#########################################################################
# Taft Calls a Special Session of Congress
#########################################################################
country_event = { 
	id = HD_USA_Political.1
	title = HD_USA_Political.1.t
	desc = HD_USA_Political.1.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# Okay.
	option = {
		name = HD_USA_Political.1.A
		ai_chance = { 
			factor = 1
		}
		add_timed_idea = {
			idea = USA_Moderately_Reduced_Tariffs
			days = 730
		}
	}
}
#########################################################################
# Taft's Southern Policy
#########################################################################
country_event = { 
	id = HD_USA_Political.2
	title = HD_USA_Political.2.t
	desc = HD_USA_Political.2.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# Okay.
	option = {
		name = HD_USA_Political.2.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.05
		add_popularity = {
			ideology = democratic
			popularity = 0.05
		}
	}
}

#########################################################################
# Taft Submits a Free Trade Agreement with Canada
#########################################################################
country_event = { 
	id = HD_USA_Political.3
	title = HD_USA_Political.3.t
	desc = HD_USA_Political.3.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# Let's hope Congress Approves...
	option = {
		name = HD_USA_Political.3.A
		ai_chance = { 
			factor = 1
		}
		random_list = {
			60 = {
				country_event = {
					id = HD_USA_Political.4
					days = 5
				}
			}
			40 = {
				country_event = {
					id = HD_USA_Political.8
					days = 5
				}
			}
		}
	}
}
#########################################################################
# FTA Canada: The Senate Approves the Bill!
#########################################################################
country_event = { 
	id = HD_USA_Political.4
	title = HD_USA_Political.4.t
	desc = HD_USA_Political.4.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# Great! Now let's just hope the Canadians agree.
	option = {
		name = HD_USA_Political.4.A
		ai_chance = { 
			factor = 1
		}
		CAN = {
			country_event = {
				id = HD_USA_Political.5
				days = 5
			}
		}
	}
}
#########################################################################
# FTA Canada POV: Taft's Free Trade Agreement
#########################################################################
country_event = { 
	id = HD_USA_Political.5
	title = HD_USA_Political.5.t
	desc = HD_USA_Political.5.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# No.
	option = {
		name = HD_USA_Political.5.A
		ai_chance = { 
			factor = 1
		}
		USA = {
			country_event = {
				id = HD_USA_Political.6
				days = 5
			}
		}
	}
	# Yes.
	option = {
		name = HD_USA_Political.5.B
		ai_chance = { 
			factor = 1
		}
		USA = {
			country_event = {
				id = HD_USA_Political.7
				days = 5
			}
		}
	}
}
#########################################################################
# FTA Canada: Canada Backslides On Our Deal
#########################################################################
country_event = { 
	id = HD_USA_Political.6
	title = HD_USA_Political.6.t
	desc = HD_USA_Political.6.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# We were so close...
	option = {
		name = HD_USA_Political.6.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.03
		add_political_power = -75
	}
}
#########################################################################
# FTA Canada: Canada Agrees To Our Deal
#########################################################################
country_event = { 
	id = HD_USA_Political.7
	title = HD_USA_Political.7.t
	desc = HD_USA_Political.7.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# Perfect!
	option = {
		name = HD_USA_Political.7.A
		ai_chance = { 
			factor = 1
		}
		add_opinion_modifier = {
			target = CAN
			modifier = USA_CAN_Free_Trade_Agreement
		}
		CAN = {
			add_opinion_modifier = {
				target = USA
				modifier = USA_CAN_Free_Trade_Agreement
			}
		}
	}
}
#########################################################################
# FTA Canada: The Senate Rejects the Bill!
#########################################################################
country_event = { 
	id = HD_USA_Political.8
	title = HD_USA_Political.8.t
	desc = HD_USA_Political.8.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# There goes our Maple Syrup...
	option = {
		name = HD_USA_Political.8.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.02
		add_political_power = -50
	}
}
#########################################################################
# The Sixteenth Amendment
#########################################################################
country_event = { 
	id = HD_USA_Political.9
	title = HD_USA_Political.9.t
	desc = HD_USA_Political.9.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# Okay.
	option = {
		name = HD_USA_Political.9.A
		ai_chance = { 
			factor = 1
		}
		add_stability = 0.02
		hidden_effect = {
			country_event = {
				id = HD_USA_Political.10
				days = 365
			}
		}
	}
}
#########################################################################
# The Sixteenth Amendment Ratified
#########################################################################
country_event = { 
	id = HD_USA_Political.10
	title = HD_USA_Political.10.t
	desc = HD_USA_Political.10.d
	picture = x
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# Okay.
	option = {
		name = HD_USA_Political.10.A
		ai_chance = { 
			factor = 1
		}
		if = {
			limit = {
				has_idea = USA_Panic_Of_1907_1
			}
			swap_ideas = {
				remove_idea = USA_Panic_Of_1907_1
				add_idea = USA_Panic_Of_1907_2
			}
		}
		else_if = {
			limit = {
				has_idea = USA_Panic_Of_1907_2
			}
			swap_ideas = {
				remove_idea = USA_Panic_Of_1907_2
				add_idea = USA_Panic_Of_1907_3
			}
		}
		else_if = {
			limit = {
				has_idea = USA_Panic_Of_1907_3
			}
			remove_idea = USA_Panic_Of_1907_3
		}
	}
}
﻿###########################
# Hour of Darkness Events : France
###########################
add_namespace = HOD_FRA_Political
#########################################################################
#  France Devolution of Church Property COUNTRY EVENT
#########################################################################
country_event = {
	id = HOD_FRA_Political.1
	title = HOD_FRA_Political.1.t
	desc = HOD_FRA_Political.1.d
	picture = GFX_news_ENG_London_Fire
	is_triggered_only = yes
	
	# An Amended Law - Historical
	option = {
		name = HOD_FRA_Political.1.A
		ai_chance = {
			factor = 100
			modifier = {
				factor = 1000
				is_historical_focus_on = yes
			}
		}
		add_political_power = 50
		complete_national_focus = FRA_Amended_Law
	}
	# No Amendments! - Socialist
	option = {
		name = HOD_FRA_Political.1.B
		ai_chance = {
			factor = 25
		}
		add_political_power = -50
		complete_national_focus = FRA_No_Amendments
	}
	# Church Property Is Church Property - Conservative/Monarchy
	option = {
		name = HOD_FRA_Political.1.C
		ai_chance = {
			factor = 25
		}
		add_political_power = -50
		complete_national_focus = FRA_Church_Property_Is_Church_Property
	}
}

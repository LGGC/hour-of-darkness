﻿###########################
# Darkest Hour Election Events : Australia
###########################
add_namespace = HD_Portugal_King_Regicide
#########################################################################
#  The Lisbon Regicide
#########################################################################
country_event = { 
	id = HD_Portugal_King_Regicide.1
	title = HD_Portugal_King_Regicide.1.t
	desc = HD_Portugal_King_Regicide.1.d
	picture = GFX_report_Macedonia
	
	fire_only_once = yes	
	trigger = {
		original_tag = POR
		date > 1908.2.1
		date < 1908.3.1
	}
	
	# Do the people even care about such brutal acts?
	option = {
		name = HD_Portugal_King_Regicide.1.A
		ai_chance = { 
			factor = 1
		}
		kill_country_leader = yes
		add_stability = -0.25
		country_event = {
			id = HD_Portugal_King_Regicide.2
			days = 3
		}
	}
}
#########################################################################
#  João Franco Dismissed as Prime Minister
#########################################################################
country_event = { 
	id = HD_Portugal_King_Regicide.2
	title = HD_Portugal_King_Regicide.2.t
	desc = HD_Portugal_King_Regicide.2.d
	picture = GFX_report_Macedonia
	
	fire_only_once = yes
	is_triggered_only = yes
	
	# If he cannot safeguard the royal family he is unfit to be Prime Minister.
	option = {
		name = HD_Portugal_King_Regicide.2.A
		ai_chance = { 
			factor = 1
		}
		swap_ideas = {
			remove_idea = POR_HoG_Joao_Franco
			add_idea = POR_HoG_Francisco_Joaquim
		}
		country_event = {
			id = HD_Portugal_King_Regicide.2
			days = 3
		}
	}
}
#########################################################################
#  Ferreira do Amaral Loses Parliamentary Support
#########################################################################
country_event = { 
	id = HD_Portugal_King_Regicide.3
	title = HD_Portugal_King_Regicide.3.t
	desc = HD_Portugal_King_Regicide.3.d
	picture = GFX_report_Macedonia
	
	fire_only_once = yes	
	trigger = {
		original_tag = POR
		date > 1908.12.26
		date < 1909.1.1
	}
	
	# May Artur de Campos Henriques bring stability to the Kingdom.
	option = {
		name = HD_Portugal_King_Regicide.3.A
		ai_chance = { 
			factor = 1
		}
		swap_ideas = {
			remove_idea = POR_HoG_Francisco_Joaquim
			add_idea = POR_HoG_Artur_Alberto
		}
	}
}
#########################################################################
#  The Lisbon Regicide NEWS EVENT
#########################################################################
news_event = {
	id = HD_Portugal_King_Regicide.4
	title = HD_Portugal_King_Regicide.4.t
	desc = HD_Portugal_King_Regicide.4.d
	picture = GFX_news_ENG_London_Fire
	show_major = {
		is_european_great_powers = yes
		original_tag = SPR
	}
	is_triggered_only = yes
	
	# Do the people even care about such brutal acts?
	option = {
		name = HD_Portugal_King_Regicide.4.A
		ai_chance = { 
			factor = 1
		}
	}
}

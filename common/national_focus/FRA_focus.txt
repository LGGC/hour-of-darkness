focus_tree = {
	id = FRA_Democracy
	country = {
		factor = 0
		modifier = {
			add = 100
			tag = FRA
		}
	}
	default = no
	continuous_focus_position = {
		x = 200
		y = 3000
	}
	focus = {
		id = FRA_The_French_Political_Situation
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 35
		x = 20
		y = 1
		completion_reward = {
			add_political_power = 100
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Ribot_Law
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 14
		x = 2
		y = 1
		relative_position_id = FRA_The_French_Political_Situation
		prerequisite = { focus = FRA_The_French_Political_Situation }
		completion_reward = {
			add_ideas = FRA_Ribot_Law_idea
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Investments_In_Russia #change to entente stuff
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 35
		x = -2
		y = 1
		relative_position_id = FRA_The_French_Political_Situation
		prerequisite = { focus = FRA_The_French_Political_Situation }
		completion_reward = {
			
		}
		search_filters = { FOCUS_FILTER_INDUSTRY }
	}
	focus = {
		id = FRA_Church_Property
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 14
		x = 0
		y = 1
		relative_position_id = FRA_The_French_Political_Situation
		prerequisite = { focus = FRA_The_French_Political_Situation }
		completion_reward = {
			country_event = {
				id = HOD_FRA_Political.1
			}
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Amended_Law
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 0
		x = 0
		y = 1
		available = {
			always = false
		}
		mutually_exclusive = {
			focus = FRA_Church_Property_Is_Church_Property
			focus = FRA_No_Amendments
		}
		relative_position_id = FRA_Church_Property
		prerequisite = { focus = FRA_Church_Property }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Church_Property_Is_Church_Property
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 0
		x = 3
		y = 1
		available = {
			always = false
		}
		mutually_exclusive = {
			focus = FRA_Amended_Law
			focus = FRA_No_Amendments
		}
		relative_position_id = FRA_Church_Property
		prerequisite = { focus = FRA_Church_Property }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_No_Amendments
		icon = GFX_GENERIC_Ideology_Socialism
		cost = 0
		x = -3
		y = 1
		available = {
			always = false
		}
		mutually_exclusive = {
			focus = FRA_Amended_Law
			focus = FRA_Church_Property_Is_Church_Property
		}
		relative_position_id = FRA_Church_Property
		prerequisite = { focus = FRA_Church_Property }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Death_Penalty_Affair
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 14
		x = -1
		y = 1
		relative_position_id = FRA_Amended_Law
		prerequisite = { focus = FRA_Amended_Law focus = FRA_No_Amendments focus = FRA_Church_Property_Is_Church_Property }
		completion_reward = {
			#event
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Famille_Insaisissable
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 14
		x = 1
		y = 1
		relative_position_id = FRA_Amended_Law
		prerequisite = { focus = FRA_Amended_Law focus = FRA_No_Amendments focus = FRA_Church_Property_Is_Church_Property }
		completion_reward = {
			#event
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Found_The_AFCM
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 35
		x = 0
		y = 2
		relative_position_id = FRA_Amended_Law
		prerequisite = { focus = FRA_Amended_Law focus = FRA_No_Amendments focus = FRA_Church_Property_Is_Church_Property }
		completion_reward = {
			#event
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Ammend_The_Labourers_Issues
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 14
		x = 0
		y = 2
		relative_position_id = FRA_Death_Penalty_Affair
		prerequisite = { focus = FRA_Death_Penalty_Affair }
		prerequisite = { focus = FRA_Found_The_AFCM }
		completion_reward = {
			#decisions
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Lower_Taxes_Lower_Class
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 14
		x = 0
		y = 2
		relative_position_id = FRA_Famille_Insaisissable
		prerequisite = { focus = FRA_Famille_Insaisissable }
		prerequisite = { focus = FRA_Found_The_AFCM }
		completion_reward = {
			#decisions
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Army_Reform
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 35
		x = 1
		y = 1
		relative_position_id = FRA_Ammend_The_Labourers_Issues
		prerequisite = { focus = FRA_Lower_Taxes_Lower_Class }
		prerequisite = { focus = FRA_Ammend_The_Labourers_Issues }
		completion_reward = {
			#decisions
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Ciemenceau_Naval_Affair
		icon = GFX_GENERIC_Ideology_Liberalism
		cost = 0
		x = 0
		y = 1
		available = {
			always = false
		}
		relative_position_id = FRA_Army_Reform
		prerequisite = { focus = FRA_Army_Reform }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_1910_Elections
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 0
		x = 0
		y = 1
		available = {
			always = false
		}
		relative_position_id = FRA_Ciemenceau_Naval_Affair
		prerequisite = { focus = FRA_Ciemenceau_Naval_Affair }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Pro_Socialism_Campaign
		icon = GFX_GENERIC_Ideology_Socialism
		cost = 35
		x = -1
		y = 1
		relative_position_id = FRA_No_Amendments
		prerequisite = { focus = FRA_No_Amendments }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Make_Deal_With_Unions
		icon = GFX_GENERIC_Ideology_Socialism
		cost = 35
		x = -1
		y = 1
		relative_position_id = FRA_Pro_Socialism_Campaign
		prerequisite = { focus = FRA_Pro_Socialism_Campaign }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Revive_Zola_Spirit
		icon = GFX_GENERIC_Ideology_Socialism
		cost = 35
		x = 1
		y = 1
		relative_position_id = FRA_Pro_Socialism_Campaign
		prerequisite = { focus = FRA_Pro_Socialism_Campaign }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Unite_All_French_Socialists
		icon = GFX_GENERIC_Ideology_Socialism
		cost = 35
		x = -1
		y = 2
		relative_position_id = FRA_Revive_Zola_Spirit
		prerequisite = { focus = FRA_Revive_Zola_Spirit }
		prerequisite = { focus = FRA_Make_Deal_With_Unions }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Pro_Conservatism_Campaign
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 35
		x = 1
		y = 1
		relative_position_id = FRA_Church_Property_Is_Church_Property
		prerequisite = { focus = FRA_Church_Property_Is_Church_Property }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Seek_Church_Pope_Support
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 35
		x = 1
		y = 1
		relative_position_id = FRA_Pro_Conservatism_Campaign
		prerequisite = { focus = FRA_Pro_Conservatism_Campaign }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Revive_Chateaubriand_Spirit
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 35
		x = -1
		y = 1
		relative_position_id = FRA_Pro_Conservatism_Campaign
		prerequisite = { focus = FRA_Pro_Conservatism_Campaign }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
	focus = {
		id = FRA_Unite_All_French_Socialists
		icon = GFX_GENERIC_Ideology_Conservatism
		cost = 35
		x = 1
		y = 2
		relative_position_id = FRA_Revive_Chateaubriand_Spirit
		prerequisite = { focus = FRA_Revive_Chateaubriand_Spirit }
		prerequisite = { focus = FRA_Seek_Church_Pope_Support }
		completion_reward = {
		}
		search_filters = { FOCUS_FILTER_POLITICAL }
	}
}

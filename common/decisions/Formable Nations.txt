#########################################################################
#  Soviet Subjects
#########################################################################
formable_nations_category = {
	###########################
	# Germany - Weimar War Flag
	###########################
	GER_Weimar_War_Flag = {
		icon = GER_Anschluss
		fire_only_once = no

		allowed = {
			original_tag = GER
		}
		visible = {
			has_war = yes
			has_cosmetic_tag = GER_WEIMAR
			NOT = { has_cosmetic_tag = GER_WEIMAR_WAR }
		}
		available = {
			has_war = yes			
		}
		cost = 10
		
		ai_will_do = {
			factor = 0
		}
		
		complete_effect = {
			set_cosmetic_tag = GER_WEIMAR_WAR
		} 
	}
	###########################
	# Germany - Weimar Peace Flag
	###########################
	GER_Weimar_Peace_Flag = {
		icon = GER_Anschluss
		fire_only_once = no

		allowed = {
			original_tag = GER
		}
		visible = {
			has_war = no
			has_cosmetic_tag = GER_WEIMAR_WAR
		}
		available = {
			has_war = no			
		}
		cost = 10
		
		ai_will_do = {
			factor = 0
		}
		
		complete_effect = {
			set_cosmetic_tag = GER_WEIMAR
		} 
	}
	###########################
	# Germany - War Flag
	###########################
	GER_War_Flag = {
		icon = GER_Anschluss
		fire_only_once = no

		allowed = {
			original_tag = GER
		}
		visible = {
			has_war = yes
			has_government = fascist
			NOT = { has_cosmetic_tag = GER_WAR       }
			NOT = { has_cosmetic_tag = GER_WAR_FIRST }
		}
		available = {
			has_war = yes			
		}
		cost = 10
		
		ai_will_do = {
			factor = 0
		}
		
		complete_effect = {
			if = {
				limit = {
					date < 1938.1.1
				}
				set_cosmetic_tag = GER_WAR_FIRST
			}
			else = {
				set_cosmetic_tag = GER_WAR
			}
		} 
	}
	###########################
	# Germany - Peace Flag
	###########################
	GER_Peace_Flag = {
		icon = GER_Anschluss
		fire_only_once = no

		allowed = {
			original_tag = GER
		}
		visible = {
			has_war = no
			has_cosmetic_tag = GER_WAR
			has_government = fascist
		}
		available = {
			has_war = no
		}
		cost = 10
		
		ai_will_do = {
			factor = 0
		}
		
		complete_effect = {
			drop_cosmetic_tag = yes
		}
	}
	###########################
	# Germany - KR War Flag
	###########################
	GER_KR_War_Flag = {
		icon = GER_Anschluss
		fire_only_once = no

		allowed = {
			original_tag = GER
		}
		visible = {
			has_war = yes
			has_cosmetic_tag = GER_KR
			has_government = authoritarian
			NOT = { has_cosmetic_tag = GER_KR_WAR }
		}
		available = {
			has_war = yes			
		}
		cost = 10
		
		ai_will_do = {
			factor = 0
		}
		
		complete_effect = {
			set_cosmetic_tag = GER_KR_WAR
		} 
	}
	###########################
	# Germany - KR Peace Flag
	###########################
	GER_KR_Peace_Flag = {
		icon = GER_Anschluss
		fire_only_once = no

		allowed = {
			original_tag = GER
		}
		visible = {
			has_war = no
			has_cosmetic_tag = GER_KR_WAR
			has_government = authoritarian
		}
		available = {
			has_war = no			
		}
		cost = 10
		
		ai_will_do = {
			factor = 0
		}
		
		complete_effect = {
			set_cosmetic_tag = GER_KR
		}
	}
}
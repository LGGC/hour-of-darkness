ideas = {
	#################################################
	### Head of Government
	#################################################
	Head_of_Government = {
		# Pyotr Stolypin
		RUS_HoG_Pyotr_Stolypin = {
			picture = Pyotr_Stolypin
			allowed = { tag = RUS }
			allowed_to_remove = { always = no }
			available = {
				date > 1906.1.1
				date < 1912.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Pyotr_Stolypin_unavailable }
			}
			traits = { ideology_A POSITION_Prime_Minister }
		}	
	}
	#################################################
	### Foreign Minister
	#################################################
	Foreign_Minister = {
		# Alexander Izvolsky
		RUS_FM_Alexander_Izvolsky = {
			picture = Alexander_Izvolsky
			allowed = { tag = RUS }
			available = {
				date > 1906.1.1
				date < 1911.5.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Alexander_Izvolsky_unavailable }
			}
			traits = { ideology_A}
		}
	}
	#################################################
	### Minister of Security
	#################################################
	Minister_of_Security = {
		# Pyotr Stolypin
		RUS_MoS_Pyotr_Stolypin = {
			picture = Pyotr_Stolypin
			allowed = { tag = RUS }
			available = {
				date > 1906.3.1
				date < 1912.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Pyotr_Stolypin_unavailable }
			}
			traits = { ideology_A }
		}
	}
	#################################################
	### Armaments Minister
	#################################################
	Armaments_Minister = {
	# Vladimir Kokovtsov
		RUS_AM_Vladimir_Kokovtsov = {
			picture = Vladimir_Kokovtsov
			allowed = { tag = RUS }
			available = {
				date > 1906.1.1
				date < 1914.2.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Vladimir_Kokovtsov_unavailable }
			}
			
			
			traits = { ideology_A }
		}
	}
	#################################################
	### Head of Intelligence
	#################################################
	Head_of_Intelligence = {
		# Alexander Vasilievich
		RUS_HoI_Alexander_Vasilievich = {
			picture = Alxander_Vasilievich
			allowed = { tag = RUS }
			available = {
				date > 1905.1.1
				date < 1910.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Alexander_Vasilievich_unavailable }
			}
			
			
			traits = { ideology_A }
		}
	}
	#################################################
	### Chief of Staff
	#################################################
	Chief_of_Staff = {
		# Henri de Lacroix
		FRA_CoStaff_Henri_de_Lacroix = {
			picture = Henri_de_Lacroix
			allowed = { OR = { tag = FRA }}
			visible = {
				date > 1907.1.1
				date < 1909.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Henri_de_Lacroix_unavailable }
			}
			
			
			traits = { ideology_D }
		}
	}
	#################################################
	### Chief of Army
	#################################################
	Chief_of_Army = {
		# Jean Brun
		FRA_CoArmy_Jean_Brun = {
			picture = Jean_Brun
			allowed = { tag = FRA }
			visible = {
				date > 1905.1.1
				date < 1910.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Brun_unavailable }
			}
			
			
			traits = { ideology_D }
		}
	}
	#################################################
	### Chief of Navy
	#################################################
	Chief_of_Navy = {
		# Charles Aubert
		FRA_CoNavy_Charles_Aubert = {
			picture = Charles_Aubert
			allowed = { OR = { tag = FRA }}
			visible = {
				date > 1900.1.1
				date < 1915.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Aubert_unavailable }
			}
			
			
			traits = { ideology_D }
		}
	}
	#################################################
	### Chief of Airforce
	#################################################
	Chief_of_Airforce = {
		# Victor Duval
		FRA_CoAir_Victor_Duval = {
			picture = Victor_Duval
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1900.1.1
				date < 1936.1.1
				F_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Duval_unavailable }
			}
			
			
			traits = { ideology_F CoAir_Air_Superiority_Doctrine }
		}
		# Jean Marie Bergeret
		FRA_CoAir_Jean_Marie_Bergeret = {
			picture = Jean_Marie_Bergeret
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1924.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Marie_Bergeret_unavailable }
			}
			
			
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
		# Maurice Jannekeyn
		FRA_CoAir_Maurice_Jannekeyn = {
			picture = Maurice_Jannekeyn
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1924.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Maurice_Jannekeyn_unavailable }
			}
			
			
			traits = { ideology_A CoAir_Carpet_Bombing_Doctrine }
		}
		# Robert Rossi
		FRA_CoAir_Robert_Rossi = {
			picture = Robert_Rossi
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Robert_Rossi_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Martial Valín
		FRA_CoAir_Martial_Valin = {
			picture = Martial_Valin
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Martial_Valin_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
		# Raymond Roques
		FRA_CoAir_Raymond_Roques = {
			picture = Raymond_Roques
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Raymond_Roques_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
		# Bernard Capillon
		FRA_CoAir_Bernard_Capillon_2 = {
			picture = Bernard_Capillon_2
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1985.1.1
				date < 2000.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bernard_Capillon_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
		# Edouard Barès
		FRA_CoAir_Edouard_Bares = {
			picture = Edouard_Bares
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1914.1.1
				date < 1936.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Edouard_Bares_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Jean Du Peuty
		FRA_CoAir_Jean_Du_Peuty = {
			picture = Jean_Du_Peuty
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1917.1.1
				date < 1936.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Jean_Du_Peuty_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Louis Guillabert
		FRA_CoAir_Louis_Guillabert = {
			picture = Louis_Guillabert
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1917.1.1
				date < 1936.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Louis_Guillabert_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
		# Victor Duval
		FRA_CoAir_Victor_Duval_2 = {
			picture = Victor_Duval_2
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1917.1.1
				date < 1936.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Duval_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
		# Laurent Eynac
		FRA_CoAir_Laurent_Eynac = {
			picture = Laurent_Eynac
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1928.1.1
				date < 1936.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Laurent_Eynac_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Paul Painlevé
		FRA_CoAir_Paul_Painleve = {
			picture = Paul_Painleve
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1930.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Paul_Painleve_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Jacques-Louis Dumesnil
		FRA_CoAir_JacquesLouis_Dumesnil = {
			picture = JacquesLouis_Dumesnil
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1931.1.1
				date < 1936.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = JacquesLouis_Dumesnil_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Auguste Edouard Hirschauer
		FRA_CoAir_Auguste_Edouard_Hirschauer = {
			picture = Auguste_Edouard_Hirschauer
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1914.1.1
				date < 1936.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Auguste_Edouard_Hirschauer_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
		# Victor Denain
		FRA_CoAir_Victor_Denain = {
			picture = Victor_Denain
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Victor_Denain_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
		# Joseph Vuillemin
		FRA_CoAir_Joseph_Vuillemin = {
			picture = Joseph_Vuillemin
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Joseph_Vuillemin_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
		# Guy La Chambre
		FRA_CoAir_Guy_La_Chambre = {
			picture = Guy_La_Chambre
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Guy_La_Chambre_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Vertical_Envelopment_Doctrine }
		}
		# Bernard Capillon
		FRA_CoAir_Bernard_Capillon = {
			picture = Bernard_Capillon
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1980.1.1
				date < 2000.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Bernard_Capillon_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Carpet_Bombing_Doctrine }
		}
		# Marcel Deat
		FRA_CoAir_Marcel_Deat = {
			picture = Marcel_Deat
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Marcel_Deat_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Air_Superiority_Doctrine }
		}
		# Pierre Cot
		FRA_CoAir_Pierre_Cot = {
			picture = Pierre_Cot
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = { has_country_flag = Pierre_Cot_unavailable }
			}
			
			
			traits = { ideology_D CoAir_Army_Aviation_Doctrine }
		}
		# Marcel Gitton
		FRA_CoAir_Marcel_Gitton = {
			picture = Marcel_Gitton
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1924.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Marcel_Gitton_unavailable }
			}
			
			
			traits = { ideology_C CoAir_Air_Superiority_Doctrine }
		}
		# Charles Tillon
		FRA_CoAir_Charles_Tillon = {
			picture = Charles_Tillon
			allowed = { OR = { tag = FRA tag = VIC }}
			visible = {
				date > 1924.1.1
				date < 1964.1.1
				C_Minister_Allowed = yes
				NOT = { has_country_flag = Charles_Tillon_unavailable }
			}
			
			
			traits = { ideology_C CoAir_Army_Aviation_Doctrine }
		}
	}
}

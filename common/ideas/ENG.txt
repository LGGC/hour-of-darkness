ideas = {
#########################################################################
#  National Spirits
#########################################################################
	country = {
		#### WW1 Starting
		ENG_The_Edwardian_Era = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				research_speed_factor = 0.05
				min_export = 0.1
				socialist_drift = 0.01
			}
		}
		ENG_Pax_Britannica = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				stability_factor = 0.10
			}
		}
		ENG_The_Irish_Home_Rule_Movement = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				political_power_factor = -0.15
				stability_factor = -0.05
			}
		}
		ENG_The_Two_Power_Standard = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				industrial_capacity_dockyard = 0.1
				production_speed_dockyard_factor = 0.05
				money_expenses_factor = 0.1
				hidden_modifier = {
					money_expenses_factor_from_ideas = 0.1
				}
			}
		}
		ENG_Ongoing_Haldane_Reforms = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				army_org_factor = -0.3
				army_morale_factor = -0.4
				army_attack_factor = -0.2
				army_defence_factor = -0.2
			}
			research_bonus = {
				land_doctrine = -0.50
			}
		}
		ENG_Discontent_Labourers = {
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				production_speed_buildings_factor = -0.1
				industrial_capacity_factory = -0.05
				production_factory_efficiency_gain_factor = -0.05
			}
		}
	}
}
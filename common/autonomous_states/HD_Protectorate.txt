autonomy_state = {
	
	id = autonomy_protectorate # For Russia
	use_overlord_color = yes
	is_puppet = yes
	
	min_freedom_level = 0.30
	
	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		can_be_spymaster = no
	}
	
	modifier = {
		autonomy_manpower_share = 0.0
		extra_trade_to_overlord_factor = 0.40
		overlord_trade_cost_factor = -0.40
		political_power_factor = -0.2
		master_ideology_drift = 0.01 # Russification Programs
	}
	
	ai_subject_wants_higher = { # This will be scripted, and we don't want AI trying cheeky stuff :)
		factor = 0.0
	}
	
	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = no
	}

	allowed = {
		OVERLORD = { original_tag = RUS }
	}

	can_take_level = {
		#trigger here
	}

	can_lose_level = {
		#trigger here
	}
}

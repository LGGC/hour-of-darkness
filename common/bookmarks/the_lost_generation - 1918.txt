bookmarks = {
	bookmark = {
		name = "THE_LOST_GENERATION"
		desc = "THE_LOST_GENERATION_DESC"
		date = 1918.11.11
		picture = "GFX_select_date_1918"
		default_country = "GER"
		
		"FRA"={
			history = "FRA_THE_LOST_GENERATION_DESC"
			ideology = democratic
			ideas = { }	
			focuses = { }
		}
		"USA"={
			history = "USA_THE_LOST_GENERATION_DESC"
			ideology = democratic
			ideas = { }	
			focuses = { }
		}
		"ENG"={
			history = "ENG_THE_LOST_GENERATION_DESC"
			ideology = democratic
			ideas = { }	
			focuses = { }
		}
		"GER"={
			history = "GER_THE_LOST_GENERATION_DESC"
			ideology = democratic
			ideas = { }	
			focuses = { }
		}
		"OTT"={
			history = "OTT_THE_LOST_GENERATION_DESC"
			ideology = authoritarian
			ideas = { }	
			focuses = { }	
		}
		"RUS"={
			history = "RUS_THE_LOST_GENERATION_DESC"
			ideology = authoritarian
			ideas = { }
			focuses = { }	
		}
		"SOV"={
			history = "SOV_THE_LOST_GENERATION_DESC"
			ideology = authoritarian
			ideas = { }	
			focuses = { }
		}

		"---"={
			history = "OTHER_THE_LOST_GENERATION_DESC"
		}
		##########################################
		## Minors
		##########################################
		"ROC"={
			minor = yes
			history = ""
			ideology = authoritarian
			ideas = {}
			focuses = {}
		}
		"ZHI"={
			minor = yes
			history = ""
			ideology = authoritarian
			ideas = {}
			focuses = {}
		}
		"CHI"={
			minor = yes
			history = ""
			ideology = authoritarian
			ideas = {}
			focuses = {}
		}

		effect = {
			randomize_weather = 22345 # <- Obligatory in every bookmark !
			#123 = { rain_light = yes }
		}
	}
}
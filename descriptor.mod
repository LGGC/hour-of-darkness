version="0.1"
supported_version="1.10.*"
name="Hour of Darkness - Dev Build"
path="mod/hour-of-darkness"
dependencies={
   " Darkest Hour"
}
tags={
	"Alternative History"
	"Gameplay"
	"Events"
	"Map"
}
replace_path="common/bookmarks"
replace_path="history/states"
replace_path="history/countries"
replace_path="common/decisions"
replace_path="common/decisions/categories"
replace_path="common/units/names_divisions"